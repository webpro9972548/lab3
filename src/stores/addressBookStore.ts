import { ref } from 'vue'
import { defineStore } from 'pinia'
interface AddressBook{
  id: number
  name: string
  tel: string
  gender: string
}
export const useAddressBookStore = defineStore('address_book', () => {

  let lastId = 1
  const address = ref<AddressBook>({
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
  })
  const addressList = ref<AddressBook[]>([])
  const isAddNew = ref(false)
  function save(){
    if(address.value.id>0){
      const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
      addressList.value[editedIndex] = address.value
    }else{
      address.value.id = lastId++
      addressList.value.push(address.value)
    }
    isAddNew.value = false
    address.value = {
        id: 0,
        name: '',
        tel: '',
        gender: 'Male'
      }
  }

  function clear(){
    isAddNew.value = false
      address.value = {
      id: 0,
      name: '',
      tel: '',
      gender: 'Male'
    }
  }

  function edit(id: number){
    isAddNew.value = true
    const editedIndex = addressList.value.findIndex((item) => item.id === id)
    address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))
    //copy Object JSON.parse(JSON.stringify())
  }

  function remove(id: number){
    const deleteIndex = addressList.value.findIndex((item) => item.id === id)
    //copy Object JSON.parse(JSON.stringify())
    addressList.value.splice(deleteIndex, 1)
  }
  return { address, addressList, save, remove, clear, edit, isAddNew }
})
